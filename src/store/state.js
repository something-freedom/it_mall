export default {
  showShade: false,
  banners: [],
  cats: [],
  plist: [],
  pdetail: {},
  actionSheetShow: false,
  commentsList: [],
  searchList: [],
  userInfo: {},
  shoucangStatus: false,
  currentCatid: 0,
  more: false,
  page: 1,
  size: 7
};
