import config from "../utils/config";
import Services from "./services";
import { request } from "../utils/index";
export default {
  // async changeIsShow({state},isShow) {
  //   state.isShow = isShow
  // }
  async getBanners({ state }) {
    const banners = await Services.getBanners();
    banners.data.map((item, index) => {
      item.image = config.imageUrl + item.image;
    });
    state.banners = banners.data;
    return banners.data;
  },
  async getCats({ state }) {
    const cats = await Services.getCats();
    state.cats = cats.data;
    return cats.data;
  },
  async getP({ state }, obj) {
    const plist = await Services.getP(obj);
    plist.data.list.map((item, index) => {
      item.image = config.imageUrl + item.image;
    });
    if(obj.title) {
      state.searchList = plist.data.list;
    } else {
      state.plist = plist.data.list;
    }
    // console.log(plist.data.list);
    return plist.data.list;
  },
  async pDetail({ state }, id) {
    const detail = await Services.pDetail(id);
    state.pdetail = detail.data;
    return detail.data;
  },
  async postComment({ state }, param) {
    const result = await Services.postComment(param);
    return result;
  },
  async getComments({ state }, obj) {
    const result = await Services.getComments(obj);
    return result;
  },
  async upvoteProduct({ state }, id) {
    const result = await Services.upvoteProduct(id);
    return result;
  },
  async readUpvote({ state }, id) {
    const result = await Services.readUpvote(id);
    return result;
  }
};
