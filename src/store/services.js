import { request } from "../utils/index";
class Services {
  //首页轮播
  getBanners() {
    return request({
      url: "/pbanners"
    });
  }
  //商品分类
  getCats() {
    return request({ url: "/pcat" });
  }
  //商品列表
  getP(obj) {
    return request({
      url: "/plist",
      data: {
        page: obj.page,
        size: obj.size,
        title: obj.title ? obj.title : '',
        catid: obj.catid ? obj.catid : ''
      }
    });
  }
  //商品详情
  pDetail(id) {
    return request({ url: `/products/read/${id}` });
  }
  //获取评论列表
  getComments(obj) {
    return request({
      url: "/pcomment",
      data: {
        id: obj.id,
        page: obj.page,
        size: obj.size
      }
    });
  }
  //给某个商品评论
  postComment(param) {
    return request({
      url: "/pcomment",
      method: "POST",
      header: {
        openid: wx.getStorageSync("userInfo").openid
      },
      data: {
        products_id: param.products_id,
        to_user_id: param.to_user_id,
        parent_id: param.parent_id,
        content: param.content
      }
    });
  }
  //给某商品点赞
  upvoteProduct(id) {
    return request({
      url: "/pupvote",
      method: "POST",
      header: {
        openid: wx.getStorageSync("userInfo").openid
      },
      data: {
        id: id
      }
    });
  }
  //某商品点赞状态
  readUpvote(id) {
    return request({
      url: "/pupvote/" + id,
      header: {
        openid: wx.getStorageSync("userInfo").openid
      }
    });
  }
}
export default new Services();
