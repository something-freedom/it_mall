import Vue from 'vue'
import App from './index'
import store from "../../store/index.js";
const app = new Vue(App)
Vue.prototype.$store = store;
app.$mount()
